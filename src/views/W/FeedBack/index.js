
import React, { useState } from "react";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "./FeedbackForm.css";
import 'bootstrap/dist/css/bootstrap.css'

const FeedbackForm = () => {
  const [serviceRating, setServiceRating] = useState(0);
  const [deliveryRating, setDeliveryRating] = useState(0);
  const [paymentRating, setPaymentRating] = useState(0);
  const [feedback, setFeedback] = useState("");
  const [showEmoji, setShowEmoji] = useState(false);

  const handleServiceRatingClick = (value) => {
    setServiceRating(value);
    setShowEmoji(value > 3);
  };

  const handleDeliveryRatingClick = (value) => {
    setDeliveryRating(value);
    setShowEmoji(value > 3);
  };

  const handlePaymentRatingClick = (value) => {
    setPaymentRating(value);
    setShowEmoji(value > 3);
  };

  const handleFeedbackChange = (event) => {
    setFeedback(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    
    toast.success("Feedback submitted successfully!", {
      position: "bottom-right",
      autoClose: 3000,
      hideProgressBar: true,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });
  };

  return (
    <div className="feedback-form">
      <h1>Feedback Form</h1>
      <div className="rating-section">
        <div className="rating-group">
          <label htmlFor="service-rating">Service:</label>
          {[1, 2, 3, 4, 5].map((value) => (
            <Star
              key={value}
              value={value}
              selected={value <= serviceRating}
              onClick={handleServiceRatingClick}
            />
          ))}
        </div>
        <div className="rating-group">
          <label htmlFor="delivery-rating">On-time Delivery:</label>
          {[1, 2, 3, 4, 5].map((value) => (
            <Star
              key={value}
              value={value}
              selected={value <= deliveryRating}
              onClick={handleDeliveryRatingClick}
            />
          ))}
        </div>
        <div className="rating-group">
          <label htmlFor="payment-rating">Product Quality:</label>
          {[1, 2, 3, 4, 5].map((value) => (
            <Star
              key={value}
              value={value}
              selected={value <= paymentRating}
              onClick={handlePaymentRatingClick}
            />
          ))}
        </div>
      </div> 
      <form onSubmit={handleSubmit}>
        <div>
          <label htmlFor="feedback">Feedback:</label>
          <textarea
            id="feedback"
            value={feedback}
            onChange={handleFeedbackChange}
          />
        </div>
        {showEmoji && (
          <div style={{ fontSize: "2rem", marginTop: "1rem" }}>😃</div>
        )}
        <button type="submit" className="btn btn-primary">Submit</button>
      </form>
      <ToastContainer />
    </div>
  );
};

const Star = ({ value, selected, onClick }) => {
    const [color, setColor] = useState("gray");
  
    const handleClick = (value) => {
      onClick(value);
      setColor(value < 3 ? "red" : "goldenrod");
    };
  
    return (
      <span
        className="star"
        style={{ cursor: "pointer", color: selected ? "goldenrod" : color }}
        onClick={() => handleClick(value)}
      >
        &#9733;
      </span>
    );
  };
  
  export default FeedbackForm;
  

