import React, { useEffect } from "react";
import { useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.css";
import { MDBTable, MDBTableHead } from "mdb-react-ui-kit";
function DisplayWInventory() {

  var [product, setProduct] = useState([]);
  const inventoryId = localStorage.getItem("inventoryId")

  useEffect(() => {
    axios.get(`http://localhost:8080/inventory/show/${localStorage.getItem("userId")}`).then((res) => {
      setProduct(res.data);
      console.log(res.data);
    }).catch((err) => {
      console.log(err);
    })
  }, [])


  const handleDelete = (id) => {

    console.log(id);
    axios.delete(`http://localhost:8080/inventory/remove/${inventoryId}/${id}`)
  };
  function GoBack() {
    navigate("/LayoutW/Cart/Buy")
  }
  return (
    <>
      <div>
        <MDBTable bordered>
          <MDBTableHead>
            <tr>
              <th className="text-center" scope="col">Manufactured On</th>
              <th className="text-center" scope="col">Expiration Date</th>
              <th className="text-center" scope="col">Quantity</th>
              <th className="text-center" scope="col">Product</th>
              <th className="text-center" scope="col">Photo</th>
              <th className="text-center" scope="col">Action</th>
            </tr>
            {product && product.items && product.items.map((item) => {
              return (

                <tr key={item.id}>
                  <td className="text-center">{item.mfgDate}</td>
                  <td className="text-center">{item.expDate}</td>
                  <td className="text-center">{item.quantity}</td>
                  <td className="text-center">{item.product && item.product.name}</td>
                  <td className="text-center"><img src={item.product.imageURL} /></td>
                  <td className="text-center">
                    <button type="button" className="btn btn-danger" onClick={() => handleDelete(item.id)}>Remove</button>
                  </td>
                </tr>
              )
            })
            }

          </MDBTableHead>
        </MDBTable>
      </div>
      <div>
        <button type="button" class="btn btn-info" onClick={GoBack}>Back</button>
      </div>
    </>
  )



}

export default DisplayWInventory;