import axios from "axios";
import { MDBTable, MDBTableHead } from "mdb-react-ui-kit";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.css'


function OnWayAddToCart() {
    const [InventoryItemList, setInventoryItemList] = useState('');
    const navigate = useNavigate();

    useEffect(() => {
        axios.get(`http://localhost:8080/inventory/getstock/${sessionStorage.getItem("pid")}`)
            .then(response => {
                console.log(response.data);
                setInventoryItemList(response.data)
            })
            .catch(error => (console.error(error)))
    }, []);

    const handleAddTocart = (id) => {
        axios.post(`http://localhost:8080/cart/add/${id}/${localStorage.getItem("cartId")}`)
            .then((res) => {
                console.log(res.data)
                sessionStorage.removeItem("inventoryItemId")
                navigate("/LayoutW/Cart/DisplayCart")
            }).catch((error) => {
                console.log(error)
            })
    }

    function GoBack() {
        navigate("/LayoutW/Cart/Buy")
    }

    return (
        <>
            <div className="FetchallProduct">
                <MDBTable bordered>
                    <MDBTableHead>

                        <tr>
                            <th className="text-center" scope="col">Manufactured Date</th>
                            <th className="text-center" scope="col">Expiration Date</th>
                            <th className="text-center" scope="col">Product</th>
                            <th className="text-center" scope="col">Price</th>
                            <th className="text-center" scope="col">Quantity</th>
                            <th className="text-center" scope="col">Action</th>
                        </tr>
                        {InventoryItemList && InventoryItemList.map((inventoryItem) => {
                            return (
                                <tr key={inventoryItem.id}>
                                    <td className="text-center">{inventoryItem.mfgDate}</td>
                                    <td className="text-center">{inventoryItem.expDate}</td>
                                    <td className="text-center"><img src={inventoryItem.product.imageURL} /></td>
                                    <td className="text-center">{inventoryItem.price}</td>
                                    <td className="text-center">{inventoryItem.quantity}</td>
                                    <td className="text-center">
                                        <button className="btn btn-primary" type="submit" onClick={() => handleAddTocart(inventoryItem.id)}>Add</button>
                                    </td>
                                </tr>
                            )
                        }
                        )}

                    </MDBTableHead>
                </MDBTable>
            </div>
            <div>
                <button type="button" class="btn btn-info" onClick={GoBack}>Back</button>
            </div>
        </>
    )

}

export default OnWayAddToCart;