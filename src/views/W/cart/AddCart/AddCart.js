import axios from "axios";
import { MDBTable, MDBTableHead } from "mdb-react-ui-kit";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.css';

function Addtocart() {
    const [product, setProduct] = useState([]);
    const [productsbyCont, setproductbyCont] = useState([]);
    const navigate = useNavigate();

    useEffect(() => {
        axios.get(`http://localhost:8080/product/byname/${sessionStorage.getItem("pname")}`)
            .then(response => (
                setProduct(response.data)
            ))
            .catch(error => (
                console.error(error)
            ))
    }, [])

    const search = () => {

        axios.get(`http://localhost:8080/product/bycontent/${sessionStorage.getItem("pname")}`)
            .then((response) => {
                setproductbyCont(response.data);
            })
            .catch((error) => {
                console.error(error);
            });
    }

    const handleNextpage = (id) => {
        sessionStorage.removeItem("pname")
        sessionStorage.setItem("pid", id)
        navigate('/LayoutW/Cart/OnWayAddToCart')
    }
    function GoBack() {
        sessionStorage.removeItem("pname")
        navigate('/LayoutW/Cart/Buy')
    }

    const handleNavigate = (productbycont) => {
        console.log(productbycont);
        setProduct(productbycont);
    }

    return (
        <>
            <div className="FetchallProduct">
                <MDBTable bordered>
                    <MDBTableHead>
                        <tr>
                            <th className="text-center" scope="col">Id</th>
                            <th className="text-center" scope="col">Name</th>
                            <th className="text-center" scope="col">Price</th>
                            <th className="text-center" scope="col">Product</th>
                            <th className="text-center" scope="col">Company</th>
                            <th className="text-center" scope="col"></th>
                        </tr>

                        <tr key={product.id}>
                            <td className="text-center">{product.id}</td>
                            <td className="text-center">{product.name}</td>
                            <td className="text-center">{product.price}</td>
                            <td className="text-center"><img src={product.imageURL} /></td>
                            <td className="text-center">{product.user && product.user.userDetails && product.user.userDetails.orgName}</td>
                            <td className="text-center">
                                <button type="submit" className="btn btn-success" onClick={() => handleNextpage(product.id)}>Buy Now</button>
                            </td>
                        </tr>
                    </MDBTableHead>
                </MDBTable>
            </div>

            <div className="FetchallProduct">
                <MDBTable bordered>
                    <MDBTableHead>
                        <tr>
                            <td className="text-center">
                                <button type="submit" className="btn btn-warning" onClick={search}>Search By Content</button>
                            </td>
                        </tr>
                        <tr>
                            <th className="text-center" scope="col">Name</th>
                            <th className="text-center" scope="col">Price</th>
                            <th className="text-center" scope="col">Product</th>
                            <th className="text-center" scope="col"> Action</th>
                        </tr>
                        {productsbyCont && productsbyCont.map((productbycont) =>
                        (
                            <tr key={productbycont.id}>
                                <td className="text-center">{productbycont.name}</td>
                                <td className="text-center">{productbycont.price}</td>
                                <td className="text-center"><img src={product.imageURL} /></td>
                                <td className="text-center">
                                    <button type="submit" className="btn btn-primary" onClick={() => handleNavigate(productbycont)}>Switch Product</button>
                                </td>
                            </tr>
                        ))}
                    </MDBTableHead>
                </MDBTable>
            </div>
            <div>
                <button type="button" className="btn btn-info text-center" onClick={GoBack}>Back</button>
            </div>
        </>
    );
}

export default Addtocart;