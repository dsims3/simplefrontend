import React, { useEffect, useState } from "react";
import axios from "axios";
import "bootstrap/dist/css/bootstrap.css";
import { useNavigate } from "react-router-dom";
import { MDBTable, MDBTableHead } from "mdb-react-ui-kit";

const Allproducts = () => {
  const [products, setProducts] = useState([]);
  const navigate = useNavigate();

  const getAllProducts = () => {
    axios.get("http://localhost:8080/product/getproducts").then((response) => {
      if (response && response.data) {
        setProducts(response.data)
      }
    }).catch(error => {
      console.log(error)
    });
  }

  useEffect(() => {
    getAllProducts()
  }, []);

  const handleNavigate = (name) => {
    console.log("pname", name);
    sessionStorage.setItem("pname", name)
    navigate('/LayoutW/Cart/AddCart')
  }

  return (
    <>
      <div className="FetchallProduct">
        <MDBTable bordered>
          <MDBTableHead>
            <tr>
              <th className="text-center" scope="col">Id</th>
              <th className="text-center" scope="col">Name</th>
              <th className="text-center" scope="col">Price</th>
              <th className="text-center" scope="col">Product</th>
              <th className="text-center" scope="col">Action</th>
            </tr>
            {products && products.map((product) =>
            (
              <tr key={product.id}>
                <td className="text-center">{product.id}</td>
                <td className="text-center">{product.name}</td>
                <td className="text-center">{product.price}</td>
                <td className="text-center"><img src={product.imageURL} /></td>
                <td className="text-center">
                  <button className="btn btn-success" onClick={() => handleNavigate(product.name)}>Add to Cart</button>
                </td>
              </tr>
            ))}
          </MDBTableHead>
        </MDBTable>
      </div>
    </>
  );
}

export default Allproducts;