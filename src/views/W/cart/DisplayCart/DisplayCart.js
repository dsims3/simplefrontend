import axios from "axios";
import { MDBTable, MDBTableHead } from "mdb-react-ui-kit";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import 'bootstrap/dist/css/bootstrap.css';

const DisplayCart = () => {
     var [cart, setcart] = useState('');
     var userId = localStorage.getItem('userId');
     const navigate = useNavigate();

     useEffect(() => {
          axios.get(`http://localhost:8080/cart/fetchcart/${userId}`)
               .then(response => {
                    console.log(response.data);
                    setcart(response.data);
               })
               .catch(error => (console.error(error)
               ))
     }, [])

     function Remove(id) {
          axios.delete(`http://localhost:8080/cart/${id}`)
               .then((response) => {
                    console.log(response.data);
                    navigate('/LayoutW/Cart/Buy');
               })
               .catch((error) => {
                    console.error(error);
               });
     }

     function handlePlaceOrder() {
          axios.post(`http://localhost:8080/order/place/${localStorage.getItem("userId")}`)
               .then(response => {
                    console.log(response.data);
                    navigate("/LayoutW/dashboard")
               })
               .catch(error => (console.error(error)
               ))
     }

     return (
          <>
               <div>
                    <MDBTable bordered>
                         <MDBTableHead>
                              <tr>
                                   <th className="text-center">Cart Total</th>
                                   <th className="text-center">Cart Total Items</th>
                                   <th className="text-center">You joined us on</th>
                              </tr>

                              <tr>
                                   <td className="text-center">{cart.total}</td>
                                   <td className="text-center">{cart.totalItems}</td>
                                   <td className="text-center">{cart.createdOn}</td>
                              </tr>
                         </MDBTableHead>
                    </MDBTable>
               </div>
               <div >
                    <MDBTable bordered>
                         <MDBTableHead>

                              <tr>
                                   <th className="text-center">Qty</th>
                                   <th className="text-center">Price</th>
                                   <th className="text-center">Product</th>
                                   <th className="text-center">Photo</th>
                                   <th className="text-center">Action</th>
                              </tr>
                              {cart && cart.cartItems && cart.cartItems.map((cartItem) => {
                                   return (
                                        <tr key={cartItem.id}>
                                             <td className="text-center">{cartItem.quantity}</td>
                                             <td className="text-center">{cartItem.price}</td>
                                             <td className="text-center">{cartItem.product.name}</td>
                                             <td className="text-center"><img src={cartItem.product.imageURL} /></td>
                                             <td className="text-center">
                                                  <button type="submit" className="btn btn-warning" onClick={() => Remove(cartItem.id)}> — </button>
                                             </td>
                                        </tr>
                                   )
                              })}

                         </MDBTableHead>
                    </MDBTable>
               </div>
               <div className="text-center">
                    <button type="submit" className="btn btn-info" onClick={handlePlaceOrder}>Place Order</button>
               </div>
          </>
     )
}

export default DisplayCart;