import React, { useEffect } from "react";
import { useState } from "react";
import axios from "axios";
import 'bootstrap/dist/css/bootstrap.css';
import { MDBTable, MDBTableHead } from "mdb-react-ui-kit";
import { useNavigate } from "react-router-dom";

function WOrderDetails() {
    var [order, setOrder] = useState([]);
    const navigate = useNavigate();
    useEffect(() => {
        axios.get("http://localhost:8080/order/" + sessionStorage.getItem("orderId")).then((res) => {
            setOrder(res.data);
            console.log(res.data);
        }).catch((error) => {
            console.log(error)
        })
    }, [])

    var handleClick = () => {
        sessionStorage.removeItem("orderId");
        navigate("/LayoutW/WOrders");
    }

    return (
        <div className="container table-responsive">
            <MDBTable bordered>
                <MDBTableHead>
                    <tr>
                        <th scope="col">Ordered Date</th>
                        <th scope="col">Delivery Date</th>
                        <th scope="col">Order Total Price</th>
                        <th scope="col">Order Items</th>
                        <th scope="col">Order Status </th>
                        <th scope="col">Order Payment Status </th>
                        <th scope="col">Order Placed by </th>
                        <th scope="col">Manufacturer </th>
                    </tr>
                    <tr key={order.id}>
                        <td>{order.orderDate}</td>
                        <td>{order.deliveryDate}</td>
                        <td>{order.orderTotal}</td>
                        <td>{order.totalOrderItems}</td>
                        <td>{order.status}</td>
                        <td>{order.payment}</td>
                        <td>{order.from && order.from.firstName}</td>
                        <td>{order.to && order.to.userDetails && order.to.userDetails.orgName}</td>
                    </tr>
                </MDBTableHead>
            </MDBTable>
            <MDBTable bordered>
                <MDBTableHead>
                    <tr>
                        <th scope="col">Product Name</th>
                        <th scope="col">Quantity</th>
                        <th scope="col">Price</th>
                    </tr>
                    {order && order.orderDetails && order.orderDetails.map((item) => {
                        return (
                            <>
                                <tr key={item.id}>
                                    <td>Product Name </td>
                                    <td>{item.product.name}</td>
                                    <td>Quantity </td>
                                    <td>{item.quantity}</td>
                                    <td>Price </td>
                                    <td>{item.price}</td>
                                </tr>
                            </>
                        )
                    })}
                    <tr>
                        <td>
                            <button type="button" className="btn btn-warning" onClick={handleClick}>Go Back</button>
                        </td>
                    </tr>
                </MDBTableHead>
            </MDBTable>
        </div >
    )
}
export default WOrderDetails;