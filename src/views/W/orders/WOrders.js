import React, { useEffect } from "react";
import { useState } from "react";
import axios from "axios";
import 'bootstrap/dist/css/bootstrap.css';
import { useNavigate } from "react-router-dom";
function WOrders() {

    var [orders, setOrders] = useState([]);
    const navigate = useNavigate();
    useEffect(() => {

        axios.get(`http://localhost:8080/order/myorders/${localStorage.getItem("userId")}`).then((res) => {
            setOrders(res.data);
            console.log(res.data);
        }).catch((error) => {
            console.log(error)
        })
    }, [])

    var handleClick = (orderId) => {
        sessionStorage.setItem("orderId", orderId);
        console.log(orderId)
        navigate("/LayoutW/WOrderDetails")
    }
    return (
        <div className="container table-responsive">
            <table className="table-responsive table-bordered table-hover">
                <tr>
                    <th style={{ paddingLeft: '10px', paddingRight: '10px' }}>
                        Order Date
                    </th>
                    <th style={{ paddingLeft: '10px', paddingRight: '10px' }}>
                        Delievry Expected on
                    </th>
                    <th style={{ paddingLeft: '10px', paddingRight: '10px' }}>
                        Order Total
                    </th>
                    <th style={{ paddingLeft: '10px', paddingRight: '10px' }}>
                        Order Status
                    </th>
                    <th style={{ paddingLeft: '10px', paddingRight: '10px' }}>
                        Payment Status
                    </th>
                    <th style={{ paddingLeft: '10px', paddingRight: '10px' }}>
                        Order Placed By
                    </th>
                    <th style={{ paddingLeft: '10px', paddingRight: '10px' }}>
                        Order Placed To
                    </th>
                    <th style={{ paddingLeft: '30px', paddingRight: '10px' }}>
                        Actions
                    </th>
                </tr>
                {orders.map((order) => {
                    return <>
                        <tr key={order.id}>
                            <td  style={{ paddingLeft: '10px',paddingRight: '10px' }} >{order.orderDate}
                            </td>
                            <td style={{ paddingLeft: '30px',paddingRight: '10px' }}>{order.deliveryDate}
                            </td>
                            <td style={{ paddingLeft: '30px',paddingRight: '10px' }}>{order.orderTotal}
                            </td>
                            <td style={{ paddingLeft: '30px',paddingRight: '10px' }}>{order.status}
                            </td>
                            <td style={{ paddingLeft: '30px',paddingRight: '10px' }}>{order.payment}
                            </td>
                            <td style={{ paddingLeft: '30px',paddingRight: '10px' }}>{order.from.firstName}
                            </td>
                            <td style={{ paddingLeft: '20px',paddingRight: '10px' }}>{order.to.userDetails.orgName}
                            </td>
                            <td style={{ paddingLeft: '10px',paddingRight: '10px' }}>
                                <button type="button" className="btn btn-primary" onClick={() => handleClick(order.id)}>See Details</button>
                            </td>
                        </tr>
                    </>
                })}
            </table>
        </div>
    )
}

export default WOrders;