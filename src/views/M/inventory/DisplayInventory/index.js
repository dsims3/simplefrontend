import React, { useEffect } from "react";
import { useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.css";
import { MDBTable, MDBTableHead } from "mdb-react-ui-kit";
function DisplayInventory() {

  var [product, setProduct] = useState([]);
  const navigate = useNavigate();
  const inventoryId = localStorage.getItem("inventoryId")

  useEffect(() => {
    axios.get(`http://localhost:8080/inventory/show/${localStorage.getItem("userId")}`).then((res) => {
      setProduct(res.data);
      console.log(res.data);

    }).catch((err) => {
      console.log(err);
    })
  }, [])




  const handleAdd = (item) => {
    sessionStorage.setItem("id", item.id);
    navigate('/DefaultLayout/inventory/AddInventory');
  };
  const handleDelete = (id) => {

    console.log(id);
    axios.delete(`http://localhost:8080/inventory/remove/${inventoryId}/${id}`)
  };

  return (
    <div>
      <MDBTable bordered>
        <MDBTableHead>
          <tr>
            <th scope="col">Manufactured On</th>
            <th scope="col">Expiration Date</th>
            <th scope="col">Quantity</th>
            <th scope="col">Product Name</th>
            <th scope="col">Product</th>
            <th scope="col">Action</th>
          </tr>
          {product && product.items && product.items.map((item) => {
            return (

              <tr key={item.id}>
                <td>{item.mfgDate}</td>
                <td>{item.expDate}</td>
                <td>{item.quantity}</td>
                <td className="text-center">{item.product && item.product.name}</td>
                <td className="text-center"><img src={ item.product && item.product.imageURL} /></td>
                <td>
                  <button type="button" className="btn btn-danger" onClick={() => handleDelete(item.id)}>Remove</button>
                  <button type="button" className="btn btn-primary" onClick={() => handleAdd(item)}>Add Another Batch</button>
                </td>

              </tr>
            )
          })
          }

        </MDBTableHead>
      </MDBTable>

    </div>
  )



}

export default DisplayInventory;