// import React from "react"
// const AddInventory=(props) => {
//     return<div>Add Inventory</div>;
// }
//   export default AddInventory;

import React, { useEffect } from "react";
import { useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { MDBTable, MDBTableHead } from "mdb-react-ui-kit";
function AddInventory(){

    var [InventoryItem,setInventoryItem]=useState({
            mfgDate:"",
            expDate:"",
            product:`${sessionStorage.getItem("id")}`,
            quantity:"",
            price:"",
            inventory:`${sessionStorage.getItem("id")}`,
    });
    const navigate = useNavigate();
   
    const cont=sessionStorage.getItem("content");
   useEffect(()=>{
    axios.post(`http://localhost:8080/inventory/add`, InventoryItem)
         .then((res)=>{(res.data)
            
          .catch(error=>(console.log(error)
          ))   
    })
   },[])
    
  const HandleAddTOInventory=()=>
  {
        //history.push("/DisplayInventory");
        navigate('/DisplayInventory'); 
  }
    return(

        <div className="container table-responsive">
            <MDBTable bordered>
                <MDBTableHead>

                 <tr>
                    <td>
                    <label className="form__label" >Manufacturing Date </label>
                    </td>
                    <td>
                    <input  type="text" name="mfgDate"  placeholder="Manufacturing Date" 
                             onChange={(event) => {
                                setInventoryItem({
                                  ...InventoryItem,
                                  mfgDate: event.target.value,
                                });
                              }}
                             className="form__input" />
                    </td>
                 </tr>

                 <tr>
                    <td>
                    <label className="form__label">Expiry Date </label>
                    </td>
                    <td>
                    <input   className="form__input" name="expDate"type="text" 
                              onChange={(event) => {
                                setInventoryItem({
                                  ...InventoryItem,
                                  expDate: event.target.value,
                                });
                              }}
                             placeholder="Expiry Date" />
                    </td>
                 </tr>
                 <tr>
                    <td>
                    <label className="form__label">Quantity </label>
                    </td>
                    <td>
                    <input   className="form__input" name="quantity"type="text" 
                               onChange={(event) => {
                                setInventoryItem({
                                  ...InventoryItem,
                                  quantity: event.target.value,
                                });
                              }}
                              placeholder="Quantity" />
                    </td>
                 </tr>
                 <tr>
                    <td>
                    <label className="form__label" >Price </label>
                    </td>
                    <td>
                    <input   className="form__input" name="price"type="text"  
                             onChange={(event) => {
                                setInventoryItem({
                                  ...InventoryItem,
                                  price: event.target.value,
                                });
                              }}
                              placeholder="Price" />
                    </td>
                 </tr>
                 <tr>
                    <td colSpan={2}>   
                        <button type="submit" className="btn btn-primary" onClick={()=>HandleAddTOInventory}>Add To Inventory</button> 
                    </td>
                 </tr>
                </MDBTableHead>
            </MDBTable>
                
        </div>
    )
}

export default AddInventory;