import React, { useEffect } from "react";
import { useState } from "react";
import axios from "axios";
import '../../style.css'
import { useNavigate } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.css";
import { MDBTable, MDBTableHead } from "mdb-react-ui-kit";
function AllProducts() {

    var [product, setProduct] = useState([]);
    const navigate = useNavigate();
    useEffect(() => {
        axios.get("http://localhost:8080/product/myproducts/"+localStorage.getItem("userId")).then((res) => {
            setProduct(res.data);
            console.log(res.data);

        }).catch((err)=>{
            console.log(err);
        })

        
    }, [])

   


    const handleUpdate = (item) => {
        sessionStorage.setItem("id",item.id);
        sessionStorage.setItem("name",item.name);
        sessionStorage.setItem("price",item.price);
        sessionStorage.setItem("imageURL",item.imageURL);
        sessionStorage.setItem("content",item.content);
       // history.push('/displayProductById');
        navigate('/DefaultLayout/Products/displayProductById');
       
      };
    const handleDelete = (id) => {
        var url="http://localhost:8080/product/".concat(id);
        console.log(id);
        axios.delete(url)
      };

      return(
        <div>
            <MDBTable bordered>
            <MDBTableHead>
              <tr>
                <th scope="col">Id</th>
                <th scope="col">Name</th>
              <th scope="col">Price</th>
              <th scope="col">Content</th>
              
              <th scope="col">Action</th>
              
             
              </tr>
              
              {
                product.map((item)=>{
                    return<>
                      <tr key={item.id}> 
                        <td>
                            <h4>
                                {item.id}
                            </h4>
                        </td>
                             <td><h4>{item.name}</h4></td>
                            

                             <td><h4>{item.price}</h4></td>
                            
                             <td><h4>{item.content}</h4></td>
                             <td>
                              <img src={item.imageURL}></img>
                             </td>
                            
                             <td>
                              
                              <button type="button" class="btn btn-danger" onClick={()=>handleDelete(item.id)} >Delete</button>
                              
                              <button type="button" class="btn btn-primary" onClick={()=>handleUpdate(item)}>Update</button>
                              
                             </td>

                            </tr>
                    </>
                })
              }
           
          </MDBTableHead>
           </MDBTable>

        </div>
      )


    
}

export default AllProducts;