import React, { useEffect } from "react";
import { useState } from "react";
import axios from "axios";
import '../../style.css'
import { useNavigate } from "react-router-dom";
function DisplayProductById(){
    const navigate = useNavigate();

    var [product,setProduct]=useState([
      
       
    ]);
   
    var pid=sessionStorage.getItem("pid");
    sessionStorage.removeItem("pid");
    const cont=sessionStorage.getItem("content");
   useEffect(()=>{
    axios.get("http://localhost:8080/product/"+pid).then((res)=>{
    setProduct(res.data);
    console.log(res.data);

})
   },[])

   const HandleUpdate=()=>{
     var url="http://localhost:8080/product/".concat(sessionStorage.getItem("id"))
     axios.put(url,product).then((res)=>{
        navigate('/DefaultLayout/Products/AllProducts');
     })
    }
    
    var HandleChange = (args) => {
        var changedProduct ={...product};
        changedProduct[args.target.name]=args.target.value;
        changedProduct[args.target.price]=args.target.value;
        changedProduct[args.target.imageURL]=args.target.value;
       
        
        setProduct(changedProduct);
        console.log(cont);
  }

   

  
    return(
        

        <div className="container table-responsive">
            <table>
                
                 <tr>

                 <td><h4>
                 <label className="form__label" for="name">Id</label>
                 <input readOnly={true} className="form__input" name="name" type="text" id="name"  value={sessionStorage.getItem("id")}/>
                 </h4>
                 </td>
                 </tr>

                 <tr>
                 <td><h4>
                 <label className="form__label" for="name">Name </label>
                 <input onChange={HandleChange} className="form__input" name="name" type="text" id="name" placeholder={sessionStorage.getItem("name")}/>
                 </h4></td>
                 </tr>

                 <br></br>
                 <tr>
                 <td><h4>
                 <label className="form__label" for="price">Price </label>
                  <input  onChange={HandleChange} type="text" name="price" id="price"  className="form__input" placeholder={sessionStorage.getItem("price")}/>
                  </h4></td>
                  </tr>

                 <br></br>

                 <tr>
                 <td><h4>
                 <label className="form__label" for="ImageURL">ImageURL </label>
                  <input  onChange={HandleChange} type="text" name="imageURL"id="ImageURL" className="form__input" placeholder={sessionStorage.getItem("imageURL")}/>
                 </h4></td>
                 </tr>

                 <br></br>

                 <tr>
                 <td><h4>
                 <label className="form__label" for="Content">Content </label>
                  <input onChange={HandleChange}  className="form__input" name="content"type="text"  id="content" value={sessionStorage.getItem("content")}/>
                 </h4></td>
                 </tr>

                 <br></br>

                 <tr>
                 <td>
                    
                    <button type="button" class="btn btn-primary" onClick={HandleUpdate}>Update</button>
                    
                 </td>


                 </tr>
                
            
              
          
            </table>
        </div>
    )
}

export default DisplayProductById;