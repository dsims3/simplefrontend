// import React from "react"
// const AddProducts=(props) => {
//     return<div>Add Products</div>;
// }
//   export default AddProducts;

import React, {useState} from 'react';
import '../../style.css';
import axios from 'axios';

function AddProducts() {
    

    let[product,setProduct]=useState({
       name: "",
       price: "",
       imageURL: "",
       content:"",
       user: {
        id: `${localStorage.getItem("userId")}`,
    }
    }) 
    function sendDataToBackend(data) {
       
        axios.post("http://localhost:8080/product", data)
          .then((response) => console.log(response.data))
          .catch((error) => console.error(error));
      }

      
        const handleClick = () => {
          console.log(product.name);
          sendDataToBackend({ product });
        };
    

        var HandleChange = (args) => {
            var changedProduct ={...product};
                    changedProduct[args.target.name]=args.target.value;
                    setProduct(changedProduct);
          }
      
    return(
      <div className="form">
          <div className="form-body">
              <div className="name">
                  <label className="form__label" for="name">Name </label>
                  <input onChange={HandleChange} className="form__input" name="name" type="text" id="name" placeholder="Name"/>
              </div>
              <div className="price">
                  <label className="form__label" for="price">Price </label>
                  <input  onChange={HandleChange} type="text" name="price" id="price"  className="form__input"placeholder="Price"/>
              </div>
              <div className="URL">
                  <label className="form__label" for="ImageURL">ImageURL </label>
                  <input  onChange={HandleChange} type="text" name="imageURL"id="ImageURL" className="form__input" placeholder="URL"/>
              </div>
              <div className="Content">
                  <label className="form__label" for="Content">Content </label>
                  <input onChange={HandleChange} className="form__input" name="content"type="text"  id="content" placeholder="Content"/>
              </div>
              
          </div>
          <div class="footer">
              {/* <button type="submit" class="btn" onClick={handleClick}>Add to Catalogue</button> */}
              <button type="submit" className="btn btn-primary" onClick={handleClick}>Add to Catalogue</button> 
                   
          </div>
      </div>      
    )       
}
export default AddProducts;