import React from 'react'
import CIcon from '@coreui/icons-react'
import {
  cilBell,
  cilCalculator,
  cilChartPie,
  cilCursor,
  cilDescription,
  cilDrop,
  cilNotes,
  cilPencil,
  cilPuzzle,
  cilSpeedometer,
  cilStar,
} from '@coreui/icons'
import { CNavGroup, CNavItem, CNavTitle, CSidebarNav } from '@coreui/react'

const _nav = [
  {
    component: CNavItem,
    name: 'Dashboard',
    to: '/LayoutW/dashboard',
    //icon: <CIcon icon={cilSpeedometer} customClassName="nav-icon" />,
   // badge: {
    //  color: 'info',
     // text: 'NEW',
    //},
  },
  // {
  //   component: CNavItem,
  //   name: 'Cart',
  //   to: '/cart',
  //   //icon: <CIcon icon={cilSpeedometer} customClassName="nav-icon" />,
  // },

  {
    component: CNavGroup,
    name: 'Cart',
    //icon: <CIcon icon={cilSpeedometer} customClassName="nav-icon" />,
    items: [
      {
        component: CNavItem,
        name: 'Cart Items',
        to: '/LayoutW/Cart/DisplayCart',
      },
      {
        component: CNavItem,
        name: 'Add Cart',
        to: '/LayoutW/Cart/Buy',
      },
    ],
  },

  {
    component: CNavGroup,
    name: 'Inventory',
    items: [
      {
        component: CNavItem,
        name: 'Display Inventory',
        to: '/LayoutW/Inventory/DisplayWInventory',
      },
    ],
    //icon: <CIcon icon={cilSpeedometer} customClassName="nav-icon" />,
  },
  {
    component: CNavItem,
    name: 'Orders',
    to: '/LayoutW/WOrders',
    //icon: <CIcon icon={cilSpeedometer} customClassName="nav-icon" />,
  },

  {
    component: CNavGroup,
    name: 'FeedBack',
    items: [
      {
        component: CNavItem,
        name: 'To Manufacturer',
        to: '/LayoutW/feedback/Tomanufacturer',
      },
    ],
    //icon: <CIcon icon={cilSpeedometer} customClassName="nav-icon" />,
  },
]
export default _nav
