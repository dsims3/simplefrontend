import React from 'react'
import { AppContent, AppSidebarW, AppFooter, AppHeader } from '../components/index'
import Data from "./Data"

const DefaultLayout = () => {

  return (
    <div>
      <AppSidebarW />
      <div className="wrapper d-flex flex-column min-vh-100 bg-light">
        <AppHeader />
        <div className="body flex-grow-1 px-3">
        <Data/>
          <AppContent />
        </div>
        <AppFooter />
      </div>
    </div>
  )
}

export default DefaultLayout
