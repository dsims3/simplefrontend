import React, { useState } from 'react'
import { useEffect } from 'react'
import axios from "axios";
import 'bootstrap/dist/css/bootstrap.css';
import '../layout/dashboard.css'
import { CButton } from '@coreui/react'
import { CCollapse } from '@coreui/react'
import { CCard, CCardBody } from '@coreui/react'

const Data = () => {

    var [data, setData] = useState([]);

    useEffect(() => {
        axios.get(`http://localhost:8080/dashboard/` + localStorage.getItem("userName"))
            .then((res) => {
                setData(res.data)
                localStorage.setItem("userId", res.data.userId)
                localStorage.setItem("inventoryId", res.data && res.data.inventory && res.data.inventory.id)
                localStorage.setItem("cartId", res.data && res.data.cart && res.data.cart.id)
            })
            .catch(error => (console.log(error)
            ))
    }, [])
    const [visible, setVisible] = useState(false)

    return (<>

        <CButton className="btn btn-primary" onClick={() => setVisible(!visible)}>Click here</CButton>
        <CCollapse visible={visible}>
            <CCard className="mt-3">
                <CCardBody>
                    <div>
                        <h4>Welcome, <span className="text-primary">{data.inventory && data.inventory.user && data.inventory.user.firstName}</span>!</h4>
                        <br></br>
                        <br></br>

                        <div className="card">
                            <div className="card-body">
                                <h4 className="h5 text-uppercase mb-1" style={{ color: 'green', fontSize: '30px' }}>Inventory Details</h4>
                                <br></br>
                                <div className="boxes-container">
                                    <div className="box">
                                        <h5>Total Quantity</h5>
                                        <p style={{ color: 'black', fontSize: '30px' }}>{data.inventory && data.inventory.totalQuantity}</p>
                                    </div>
                                    <div className="box">
                                        <h5>Inventory Worth</h5>
                                        <p style={{ color: 'black', fontSize: '30px' }}>{data.inventory && data.inventory.totalPrice}</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <br /><br /><br />
                        <div className="card">
                            <div className="card-body">
                                <h4 className="h5 text-uppercase mb-1" style={{ color: 'green', fontSize: '30px' }}>Order Details</h4 >
                                <div className="boxes-container">
                                    <div className="box">
                                        <h5>Total Orders</h5>
                                        <p style={{ color: 'black', fontSize: '30px' }}>{data.totalOrders}</p>
                                    </div>
                                    <div className="box">
                                        <h5>Order Quantity</h5>
                                        <p style={{ color: 'black', fontSize: '30px' }}>{data.totalOrdersQty}</p>
                                    </div>
                                    <div className="box">
                                        <h5>Order Price</h5>
                                        <p style={{ color: 'black', fontSize: '30px' }}>{data.totalOrdersPrice}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br /><br /><br />
                        <div className="card">
                            <div className="card-body">
                                <h4 className="h5 text-uppercase mb-1" style={{ color: 'green', fontSize: '30px' }}>Placed Order Details</h4>
                                <div className="boxes-container">
                                    <div className="box">
                                        <h5>Placed Orders</h5>
                                        <p style={{ color: 'black', fontSize: '30px' }}>{data.placedOrders}</p>
                                    </div>
                                    <div className="box">
                                        <h5>Order Placed Qty</h5>
                                        <p style={{ color: 'black', fontSize: '30px' }}>{data.placedOrdersQty}</p>
                                    </div>
                                    <div className="box">
                                        <h5>Order Placed Price</h5>
                                        <p style={{ color: 'black', fontSize: '30px' }}>{data.placedOrdersPrice}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br /><br /><br />
                        <div className="card">
                            <div className="card-body">
                                <h4 className="h5 text-uppercase mb-1" style={{ color: 'green', fontSize: '30px' }}>Delivered Order Details</h4>
                                <div className="boxes-container">
                                    <div className="box">
                                        <h5>Delivered Orders</h5>
                                        <p style={{ color: 'black', fontSize: '30px' }}>{data.deliveredOrders}</p>
                                    </div>
                                    <div className="box">
                                        <h5>Order Delivered Qty</h5>
                                        <p style={{ color: 'black', fontSize: '30px' }}>{data.deliveredOrdersQty}</p>
                                    </div>
                                    <div className="box">
                                        <h5>Order Delivered Price</h5>
                                        <p style={{ color: 'black', fontSize: '30px' }}>{data.deliveredOrdersPrice}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </CCardBody>
            </CCard>
        </CCollapse>
    </>)
}

export default Data;