import React from 'react'
import { AppContent, AppSidebar, AppFooter, AppHeader } from '../components/index'
import Navbar from '../components/Navbar'
import CommonContent from 'src/components/CommonContent'

const CommonLayout = () => {
  return (
    <div>
      
        
        <Navbar/>
        <div className="body flex-grow-1 px-3">
          <CommonContent />
        </div>
        <AppFooter />
      </div>
   
  )
}

export default CommonLayout
