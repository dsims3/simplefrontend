import React, { useEffect } from 'react'
import { useState } from 'react';
import jwtDecode from 'jwt-decode';
import 'bootstrap/dist/css/bootstrap.css';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';


function Login() {
  const [user, setUser] = useState({ email: "", password: "" });
  const [error, setError] = useState(null);
  var navigate = useNavigate();

  let isLoggedIn = localStorage.getItem("isLoggedIn")
  let token = localStorage.getItem("token");
  let role = localStorage.getItem("role");

  useEffect(() => {
    if (isLoggedIn && token !== "") {
      role = jwtDecode(token).role
      if (role == 'MANUFACTURER') {

        navigate('/DefaultLayout');
        //<DefaultLayout />


      } else if (role == 'WHOLESALER') {

        navigate('/LayoutW');
        // <LayoutW />
      } else {
        setError('Invalid role');
      }
    }
    else {
      console.log("in here");
    }
  }, [])

  var handleChange = (args) => {
    var changedUser = { ...user };
    changedUser[args.target.name] = args.target.value;
    setUser(changedUser);
  }

  const signIn = () => {
    axios
      .post("http://localhost:8080/authenticate", user)
      .then((response) => {
        console.log(response)
        if (response.data != "") {
          token = response.data;
          role = jwtDecode(token).role;
          const validUserName = jwtDecode(token).name;
          localStorage.setItem("isLoggedIn", true)
          localStorage.setItem("userName", validUserName)
          localStorage.setItem("token", token);
          console.log(token);

          if (role == 'MANUFACTURER') {
            //<DefaultLayout />
            navigate('/DefaultLayout');

          } else if (role == 'WHOLESALER') {
            //<LayoutW />
            navigate('/LayoutW');

          } else {
            setError('Invalid role');
          }
        }
      }).catch((error) => setError(error));
  }


  return (
    <>
      <section className="section background-image">
        <div className="container py-5 h-100">
          <div className="row d-flex justify-content-center align-items-center h-100">
            <div className="col-12">

              <div className="card card-registration card-registration-2">
                <div className="div1">

                  <h2 className="heading">Sign In</h2>
                  <form className="signIn" id="signIn" onSubmit={signIn}>
                    <div className="login__field">
                      <i className="login__icon zmdi zmdi-email"></i>
                      <input required type="text" className="login__input" name="email"
                        onChange={handleChange} placeholder=" Email" />
                    </div>
                    <div className="login__field">
                      <i className="login__icon zmdi zmdi-key"></i>
                      <input required type="password" className="login__input" name="password"
                        onChange={handleChange} placeholder="Password" />
                    </div>
                    <button className="button login__submit" type='submit'>
                      <span className="button__text">Log In Now</span>
                      <i className="button__icon fas fa-chevron-right"></i>
                    </button>

                  </form>
                  <br />
                  {/* <a href='#' className="forgot-text">Forgot Password ?</a> */}
                  <div className="social-login">
                    <a href="#" className="social-login__icon zmdi zmdi-instagram"></a>
                    <a href="#" className="social-login__icon zmdi zmdi-facebook-box"></a>
                    <a href="#" className="social-login__icon zmdi zmdi-twitter-box"></a>

                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  )
}

export default Login