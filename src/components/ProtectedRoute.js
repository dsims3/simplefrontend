import { Route } from "react-router-dom";
import Login from "./Login";

function ProtectedRoute(props) {
    debugger;
    var isLoggedIn = false; //Code is yet to be written

    var isLoggedInFromSessionStorage = localStorage.getItem("isLoggedIn");
    if (isLoggedInFromSessionStorage != null) {
        if (isLoggedInFromSessionStorage == "true") {
            isLoggedIn = true;
        }
    }


    if (isLoggedIn) //check for sessionStorage values
    {
        return <Route path={props.path} exact
            component={props.component} />;
    }
    else {
        return <Login></Login>
    }
}

export default ProtectedRoute;