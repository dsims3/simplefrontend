import React, { Suspense, useState } from 'react'
import { useEffect } from 'react'
import { Navigate, Route, Routes } from 'react-router-dom'
import { CContainer, CSpinner } from '@coreui/react'
import axios from "axios";
import 'bootstrap/dist/css/bootstrap.css';
import routes from '../routes'
import '../layout/dashboard.css'
const AppContent = () => {
 

  const titleStyleRed = {
    color: "red"
  };
  const titleStyleG = {
    color: "green"
  };
  const titleStyleB = {
    color: "blue"
  };
  return (<>
    
    <CContainer lg>
      <Suspense fallback={<CSpinner color="primary" />}>
        <Routes>
          {routes.map((route, idx) => {
            return (
              route.element && (
                <Route
                  key={idx}
                  path={route.path}
                  exact={route.exact}
                  name={route.name}
                  element={<route.element />}
                />
              )
            )
          })}
          <Route path="/" element={<Navigate to="dashboard" replace />} />
          <Route path="/Cart/DisplayCart" element={<Navigate to="dashboard" replace />} />
        </Routes>
      </Suspense>
    </CContainer>
  </>
  )
}

export default React.memo(AppContent);