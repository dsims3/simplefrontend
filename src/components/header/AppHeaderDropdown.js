import React from 'react';
import { useNavigate } from 'react-router-dom';
import {
  CAvatar,
  CBadge,
  CDropdown,
  CDropdownDivider,
  CDropdownHeader,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
} from '@coreui/react';
import {
  cilBell,
  cilCreditCard,
  cilCommentSquare,
  cilEnvelopeOpen,
  cilFile,
  cilLockLocked,
  cilSettings,
  cilTask,
  cilUser,
} from '@coreui/icons';
import CIcon from '@coreui/icons-react';
import avatar10 from './../../assets/images/avatars/10.jpg';



const AppHeaderDropdown = () => {
  

  const navigate = useNavigate();

  const handleLogout = () => {
      localStorage.setItem("isLoggedIn", false)
          localStorage.setItem("userName", "")
          localStorage.setItem("token", "");
          localStorage.setItem("userId", "");
          localStorage.setItem("inventoryId", "");
          localStorage.setItem("cartId", "");

    navigate('/Login');
  };
  return (
    <>
      <CDropdown variant="nav-item">
        <CDropdownToggle placement="bottom-end" className="py-0" caret={false}>
          <CAvatar src={avatar10} size="md" />
        </CDropdownToggle>
        <CDropdownMenu className="pt-0" placement="bottom-end">
          <CDropdownHeader className="bg-light fw-semibold py-2">Account</CDropdownHeader>
          <CDropdownItem href="#">
            <CIcon icon={cilLockLocked} className="me-2" />
            <button type="submit" className="btn btn-primary" onClick={handleLogout}>
              Log Out
            </button>
          </CDropdownItem>
        </CDropdownMenu>
      </CDropdown>
    </>
  );
};

export default AppHeaderDropdown;
