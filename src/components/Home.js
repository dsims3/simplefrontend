import axios from 'axios';
import { MDBTable, MDBTableHead } from 'mdb-react-ui-kit';
import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';


const Home = () => {
    const [searchTerm, setSearchTerm] = useState('');
    const [productsbyName, setproductbyName] = useState('');
    const navigate=useNavigate();
    const handlesearch = () => {

        axios.get(`http://localhost:8080/product/bycontent/`+searchTerm)
            .then((response) => {
                setproductbyName(response.data);
            })
            .catch((error) => {
                console.error(error);
            });
    }

    

    const handleNavigate = () => {
        
        navigate('/Login');
    }
    return (
        <>
            <section className='section background-image'>
            
           <div className='HomeDiv'> 
                <h2 className="heading"> WELCOME </h2>
                <h3 className='thought'>“The first wealth is health.”</h3>
                <h5>– Ralph Waldo Emerson</h5>

            <div className="Homesearch">
                 <i className="Homesearch-icon zmdi zmdi-search"></i>
                 <input
                        type="text"
                        className="login__input"
                        value={searchTerm}
                        onChange={(e) =>setSearchTerm(e.target.value)}
                        placeholder="Enter product name"
                    />
            </div>
                 <button type='submit' className='btn btn-warning' onClick={handlesearch}>Search</button>
            </div>

            <div className="FetchallProduct">
                <MDBTable bordered>
                    <MDBTableHead>
                        {productsbyName && productsbyName.map((productbyName) =>
                        (
                            <tr key={productbyName.id}>
                                <td>{productbyName.id}</td>
                                <td>{productbyName.name}</td>
                                <td>{productbyName.price}</td>
                                <td><img src={productbyName.imageURL} /></td>
                                <td>
                                    <button type="submit" className='btn btn-info' onClick={()=>handleNavigate()}>Order</button>
                                </td>
                            </tr>
                        ))}

                    </MDBTableHead>
                </MDBTable>
            </div>
           </section>

        
        </>

    )

}

export default Home