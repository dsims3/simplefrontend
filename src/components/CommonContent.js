// import React, { Suspense } from 'react';
// import { Navigate, Route, Routes } from 'react-router-dom';
// import { CContainer, CSpinner } from '@coreui/react';
// import Navbar from './Navbar';
// import Home from './Home';
// import Login from './Login';
// import About from './About';
// import Signup from './Signup';
// import Contact from './Contact';
// import ProtectedRoute from './ProtectedRoute';
// import DefaultLayout from 'src/layout/DefaultLayout';
// import LayoutW from 'src/layout/LayoutW';

// // routes config
// import routes from '../routes';

// const CommonContent = () => {
//   return (
//     <>
//       <Navbar />
//       <Routes>
//         <Route path="/" element={<Home />} />
//         <Route path="/home" element={<Home />} />
//         <Route path="/about" element={<About />} />
//         <Route path="/login" element={<Login />} />
//         <Route path="/signup" element={<Signup />} />
//         <ProtectedRoute path="/contact" element={<Contact />} />
//         <ProtectedRoute path="/mandashboard" element={<DefaultLayout />} />
//         {/* Update following code for Wholesaler dashboard */}
//         <ProtectedRoute path="/inventory" element={<LayoutW />} />
//       </Routes>
//     </>
//   );
// };

// export default CommonContent;


// import React, { Suspense } from 'react';
// import { Navigate, Route, Routes } from 'react-router-dom';
// import { CContainer, CSpinner } from '@coreui/react';
// import Navbar from './Navbar';
// import Home from './Home';
// import Login from './Login';
// import About from './About';
// import Signup from './Signup';
// import Contact from './Contact';
// import DefaultLayout from 'src/layout/DefaultLayout';
// import LayoutW from 'src/layout/LayoutW';

// // routes config
// import routes from '../routes';

// const isAuthenticated = false; // Replace with your actual authentication logic

// const CommonContent = () => {
//   return (
//     <>
//       <Navbar />
//       <Routes>
//         <Route path="/" element={<Home />} />
//         <Route path="/home" element={<Home />} />
//         <Route path="/about" element={<About />} />
//         <Route path="/login" element={<Login />} />
//         <Route path="/signup" element={<Signup />} />
//         {isAuthenticated ? (
//           <>
//             <Route path="/contact" element={<Contact />} />
//             <Route path="/mandashboard" element={<DefaultLayout />} />
//             <Route path="/inventory" element={<LayoutW />} />
//           </>
//         ) : (
//           <Navigate to="/login" replace />
//         )}
//       </Routes>
//     </>
//   );
// };

// export default CommonContent;

import React, { Suspense } from 'react';
import { Route, Routes, useNavigate } from 'react-router-dom';
import { CContainer, CSpinner } from '@coreui/react';
import Navbar from './Navbar';
import Home from './Home';
import Login from './Login';
import About from './About';
import Signup from './Signup';
import Contact from './Contact';
import DefaultLayout from 'src/layout/DefaultLayout';
import LayoutW from 'src/layout/LayoutW';
import './First.css';

// routes config
import routes from '../routes';

const isAuthenticated = false; // Replace with your actual authentication logic

const CommonContent = () => {
  const navigate = useNavigate();

  return (
    <>
      {/* <Navbar /> */}
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/home" element={<Home />} />
        <Route path="/about" element={<About />} />
        <Route path="/login" element={<Login />} />
        <Route path="/signup" element={<Signup />} />
        <Route path="/contact" element={<Contact />} />
        {isAuthenticated ? (
          <>
            
            <Route path="/defaultlayout" element={<DefaultLayout />} />
            <Route path="/layoutw" element={<LayoutW />} />
          </>
        ) : (
          () => navigate('/login', { replace: true })
        )}
      </Routes>
    </>
  );
};

export default CommonContent;

