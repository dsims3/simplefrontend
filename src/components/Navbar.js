import React from 'react'
//import logo from "../assets/brand/logosvgblack.svg";
import logo from "../assets/brand/cover.png";
import 'bootstrap/dist/css/bootstrap.css';


const Navbar = () => {
  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <a className="navbar-brand" href="#">
          <img src={logo} alt="logo" height={45}/>
        </a>
        {/* <a className="navbar-brand" href="#">
          <Logo alt="logo" />
        </a> */}
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>

        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav ms-auto">
            <li className="nav-item active">
              <a className="nav-link" href="/">Home <span className="sr-only"></span></a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="/about">About</a>
            </li>

            <li className="nav-item">
              <a className="nav-link" href="/contact">Contact</a >
            </li>

            <li className="nav-item">
              <a className="nav-link" href="/login">Login</a>
            </li>

            <li className="nav-item">
              <a className="nav-link" href="/signup">Sigup</a>
            </li>
          </ul>
        </div>
      </nav>
    </>
  )

}

export default Navbar