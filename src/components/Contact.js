import React from 'react'

import 'bootstrap/dist/css/bootstrap.css';

function Contact (){
 return(
   <>
        <section className="section background-image">
        <div className="container py-5 h-100">
       <div className="row d-flex justify-content-center align-items-center h-100">
       <div className="col-12">
          <div class="card card-registration card-registration-2">
            <div className="div1">
         
                  <h2 className="heading">Contact <br/><br/></h2>
                
                  <div class="d-flex p-2 bd-highlight">
                                      Email : medconnect@abc.com <br/><br/></div>
                                      
                 
                 
                  {/* <div class="d-flex p-2 bd-highlight">
                    Phone
                </div> */}

                
                <div class="d-flex p-2 bd-highlight">
                    Address : Hinjawadi - Kasarsai Rd, Phase 2, Hinjewadi Rajiv Gandhi Infotech Park, Hinjawadi, Pimpri-Chinchwad, Maharashtra 411057
                    
                </div>
               
                {/* <div style={{height: '10px', width: '10px'}}>
                <img src={map} alt="map" />
                </div> */}
                
                 
                
                
                  <br/>
                 
                  <div class="social-login">
                      <a href="#" class="social-login__icon zmdi zmdi-instagram"></a>
                      <a href="#" class="social-login__icon zmdi zmdi-facebook-box"></a>
                      <a href="#" class="social-login__icon zmdi zmdi-twitter-box"></a>
                   
                  </div>

            </div>
            </div>
            </div>
            </div>
            </div>
        </section>
   </>
 )
}

export default Contact