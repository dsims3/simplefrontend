import React, { useState } from 'react'
import 'bootstrap/dist/css/bootstrap.css'
import { toast } from "react-toastify";
import axios from 'axios';
import { useNavigate } from 'react-router-dom';

function Signup() {
  const navigate = useNavigate();
  let [user, setUser] = useState({
    firstName: "",
    lastName: "",
    email: "",
    password: "",
    userDetails: {
      orgName: "",
      gstNumber: "",
      lane: "",
      city: "",
      taluka: "",
      district: "",
      state: "",
      pincode: ""
    },
    phone: "",
    role: ""
  })

  const handleSubmit = (props) => {
    const token = sessionStorage.getItem("token");

    if (token != null)
      axios.defaults.headers.common["Authorization"] = "Bearer " + token;
    // check if user has really entered any value

    if (setUser.length === 0) {
      toast.error("please enter valid details");
    } else {
      axios
        .post("http://localhost:8080/user/register", user)
        .then((response) => {
          const result = response.data;

          if (result["status"] === "error") {
            toast.error("invalid details");
          } else {
            toast.success("successfully registered user");
            localStorage.setItem("token", "");
            localStorage.setItem("isLoggedIn", false);
            localStorage.setItem("userName", "");
            navigate('/login');
          }
        })
        .catch((error) => {
          
        });
    }

  };



  return (
    <>
      <section className='section background-image'>
        <div className="container py-5 h-100">
          <div className="row d-flex justify-content-center align-items-center h-100">
            <div className="col-12">
              <div className="card card-registration card-registration-2">
                <div className="card-body p-0">
                  <div className="row g-0">
                    <div className="col-lg-6">
                      <div className="p-5">
                        <h3 className="fw-normal mb-5" >General Infomation</h3>
                        <div className="login__signupfield">
                          <input type="text" className="login__input"
                            onChange={(event) => {
                              setUser({
                                ...user,
                                firstName: event.target.value,
                              });
                            }}
                            placeholder="First Name" required />
                        </div>
                        <br />
                        <div className="login__signupfield">

                          <input type="text" className="login__input"
                            onChange={(event) => {
                              setUser({
                                ...user,
                                lastName: event.target.value,
                              });
                            }}
                            placeholder="Last Name" required />
                        </div>
                        <br />

                        <div className="login__signupfield">

                          <input type="email" className="login__input"
                            onChange={(event) => {
                              setUser({
                                ...user,
                                email: event.target.value,
                              });
                            }}
                            placeholder="Email" required />
                        </div>
                        <br />

                        <div className="login__signupfield">

                          <input type="password" className="login__input"

                            onChange={(event) => {
                              setUser({
                                ...user,
                                password: event.target.value,
                              });
                            }}
                            placeholder="Password" required />
                        </div>
                        <br />
                        <div className="login__signupfield">

                          <input type="text" className="login__input"

                            onChange={(event) => {
                              setUser({
                                ...user,
                                userDetails: {
                                  ...user.userDetails,
                                  orgName: event.target.value,
                                },
                              });
                            }}
                            placeholder="Organization Name" required />
                        </div>
                        <br />

                        <div className="login__signupfield">

                          <input type="text" className="login__input" name='gstNumber'
                            onChange={(event) => {
                              setUser({
                                ...user,
                                userDetails: {
                                  ...user.userDetails,
                                  gstNumber: event.target.value,
                                },
                              });
                            }}
                            placeholder="GST Number" required />
                        </div>

                        <br />
                        <br />


                        <div className="mb-4 pb-2">
                          <select className="select"
                            onChange={(event) => {
                              setUser({
                                ...user,
                                role: event.target.value,
                              });
                            }}
                            required>
                            <option value="0">Role</option>

                            <option value="MANUFACTURER">MANUFACTURER</option>
                            <option value="WHOLESALER">WHOLESALER</option>
                          </select>
                        </div>

                      </div>
                    </div>
                    <div className="col-lg-6 bg-indigo text-white">
                      <div className="p-5">
                        <h3 className="fw-normal mb-5">Contact Details</h3>
                        <div className="login__signupfield">

                          <input type="text" className="login__input"
                            onChange={(event) => {
                              setUser({
                                ...user,
                                userDetails: {
                                  ...user.userDetails,
                                  state: event.target.value,
                                },
                              });
                            }}
                            placeholder="State" required />
                        </div>
                        <br />

                        <div className="login__signupfield">

                          <input type="text" className="login__input"
                            onChange={(event) => {
                              setUser({
                                ...user,
                                userDetails: {
                                  ...user.userDetails,
                                  district: event.target.value,
                                },
                              });
                            }}
                            placeholder="District" required />
                        </div>
                        <br />

                        <div className="login__signupfield">

                          <input type="text" className="login__input"
                            onChange={(event) => {
                              setUser({
                                ...user,
                                userDetails: {
                                  ...user.userDetails,
                                  taluka: event.target.value,
                                },
                              });
                            }}
                            placeholder="Taluka" required />
                        </div>
                        <br />

                        <div className="row">
                          <div className="col-md-5 mb-4 pb-2">
                            <div className="login__signupfield">

                              <input required type="text" className="login__input"
                                onChange={(event) => {
                                  setUser({
                                    ...user,
                                    userDetails: {
                                      ...user.userDetails,
                                      pincode: event.target.value,
                                    },
                                  });
                                }}
                                placeholder="Pin Code" />
                            </div>


                          </div>
                          <div className="col-md-7 mb-4 pb-2">

                            <div className="login__signupfield">
                              <input type="text" className="login__input"
                                onChange={(event) => {
                                  setUser({
                                    ...user,
                                    userDetails: {
                                      ...user.userDetails,
                                      city: event.target.value,
                                    },
                                  });
                                }}
                                placeholder="city" required />
                            </div>
                          </div>
                        </div>

                        <div className="login__signupfield">
                          <input type="text" className="login__input"
                            onChange={(event) => {
                              setUser({
                                ...user,
                                userDetails: {
                                  ...user.userDetails,
                                  lane: event.target.value,
                                },
                              });
                            }}
                            placeholder="lane"
                            required />
                        </div>
                        <br />
                        <div className="login__signupfield">

                          <input type="text" className="login__input"

                            onChange={(event) => {
                              setUser({
                                ...user,
                                phone: event.target.value,
                              });
                            }}
                            placeholder="Phone" required />
                        </div>
                        <br />

                        <div className="form-check d-flex justify-content-start mb-4 pb-3">
                          <input className="form-check-input me-3" type="checkbox" value="" id="form2Example3c" required />
                          <label className="form-check-label text-white" >
                            I accept the <a href="#!" className="text-white"><u>Terms and Conditions</u></a> of your
                            site.
                          </label>
                        </div>

                        <button type="submit" onClick={handleSubmit} className="btn btn-light btn-lg"
                          data-mdb-ripple-color="dark" >Register</button>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  )

}

export default Signup;