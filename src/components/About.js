 import React from 'react'

 import 'bootstrap/dist/css/bootstrap.css';

 function About (){
  return(
    <>
         <section className="section background-image">
         <div className="container py-5 h-100">
        <div className="row d-flex justify-content-center align-items-center h-100">
        <div className="col-12">
           <div class="card card-registration card-registration-2">
             <div className="div1">
          
                   <h2 className="heading">About</h2>
                 
                   <div class="d-flex p-2 bd-highlight">
                                       The Drug Supply and Inventory Management System is a web-based solution that simplifies the drug supply process between manufacturers and wholesalers. Its intuitive interface and comprehensive features allow manufacturers to add and sell products, and wholesalers to search and place orders with ease. 
                                       </div>
                  
                  
                   <div class="d-flex p-2 bd-highlight">
                  Similar products are suggested based on the contents of product, other features such as bill generation, stock tracking based on quantity and expiration date, and custom sales reports are also provided. The use of modern web technologies ensures a fast and responsive user experience. Overall, the system offers a reliable and efficient platform for managing drug supply and inventory, improving the efficiency and profitability of the pharmaceutical supply chain.
                  </div>
                 
                   <br/>
                  
                   <div class="social-login">
                       <a href="#" class="social-login__icon zmdi zmdi-instagram"></a>
                       <a href="#" class="social-login__icon zmdi zmdi-facebook-box"></a>
                       <a href="#" class="social-login__icon zmdi zmdi-twitter-box"></a>
                    
                   </div>

             </div>
             </div>
             </div>
             </div>
             </div>
         </section>
    </>
  )
 }

 export default About