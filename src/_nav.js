import { CNavGroup, CNavItem } from '@coreui/react'


const _nav = [
  {
    component: CNavItem,
    name: 'Dashboard',
    to: '/DefaultLayout/dashboard',
    //icon: <CIcon icon={cilSpeedometer} customClassName="nav-icon" />,
   // badge: {
    //  color: 'info',
     // text: 'NEW',
    //},
  },
  {
    component: CNavGroup,
    name: 'Products',
    //icon: <CIcon icon={cilSpeedometer} customClassName="nav-icon" />,
    items: [
      {
        component: CNavItem,
        name: 'All Products',
        to: '/DefaultLayout/Products/AllProducts',
      },
      {
        component: CNavItem,
        name: 'Add Products',
        to: '/DefaultLayout/Products/AddProducts',
      }
    ],
  },

  {
    component: CNavGroup,
    name: 'Inventory',
    items: [
      {
        component: CNavItem,
        name: 'Display Inventory',
        to: '/DefaultLayout/Inventory/DisplayInventory',
      },
      {
        component: CNavItem,
        name: 'Add Inventory',
        to: '/DefaultLayout/Inventory/AddInventory',
      },
    ],
    //icon: <CIcon icon={cilSpeedometer} customClassName="nav-icon" />,
  },

  {
    component: CNavItem,
    name: 'Orders',
    to: '/DefaultLayout/MOrders',
    //icon: <CIcon icon={cilSpeedometer} customClassName="nav-icon" />,
  }
]
export default _nav
